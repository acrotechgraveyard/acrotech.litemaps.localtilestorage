﻿using System;
using System.IO;
using System.Linq;
using Acrotech.LiteMaps.Engine;
using Acrotech.LiteMaps.Engine.Sources;
using Acrotech.LiteMaps.Engine.Storage;
using Acrotech.PortableLogAdapter;

#if !PocketPC
using Acrotech.PortableIoCAdapter;
#endif

namespace Acrotech.LiteMaps.Storage
{
    public class LocalTileStorage : ITileStorage
    {
        private static readonly ILogger Logger = Services.LogManager.GetLogger<LocalTileStorage>();

#if PocketPC
        public static string CurrentDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
#endif

        public static readonly DirectoryInfo DefaultStorageDirectory = new DirectoryInfo(Path.Combine(
#if PocketPC
            CurrentDirectory
#else
            Directory.GetCurrentDirectory()
#endif
            , "Tiles"));

        public LocalTileStorage(
#if !PocketPC
            IContainer container, 
#endif
            DirectoryInfo localStorage
#if !PocketPC
                = null
#endif
        )
        {
            LocalStorage = localStorage ?? DefaultStorageDirectory;
        }

#if PocketPC
        public LocalTileStorage()
            : this(DefaultStorageDirectory)
        {
        }
#endif

        public DirectoryInfo LocalStorage { get; set; }

        public virtual FileInfo GetImageFile(TileImageSource source, Tile tile)
        {
            return LocalStorage == null ? null : new FileInfo(GetImageUri(source, tile).LocalPath);
        }

        private Uri GetImageUri(TileImageSource source, Tile tile)
        {
            var baseUri = new Uri(LocalStorage.FullName + Path.DirectorySeparatorChar);
            return new Uri(baseUri, source.GetTileStorageRelativePath(tile));
        }

        private void CreateDirectory(FileInfo file)
        {
            if (file != null)
            {
                var dir = file.Directory;

                if (dir.Exists == false)
                {
                    Logger.Debug("Creating Directory: {0}", dir.FullName);

                    dir.Create();
                }
            }
        }

        #region ITileStorage Members

        public void Initialize()
        {
            Logger.Info("Initializing Local Tile Storage: {0}", LocalStorage.FullName);
        }

        public bool SaveTileImage(TileImageSource source, Tile tile, byte[] content)
        {
            var saved = false;

            try
            {
                if (content != null && content.Any())
                {
                    var file = GetImageFile(source, tile);

                    if (file != null)
                    {
                        CreateDirectory(file);

                        Logger.Debug("Saving Image File... {0}", file.FullName);

                        using (var fs = file.OpenWrite())
                        {
                            fs.Write(content, 0, content.Length);
                        }

                        saved = true;

                        Logger.Info("Image File Saved: {0}", file.FullName);
                    }
                }
                else
                {
                    Logger.Warn("Image Content is Empty");
                }
            }
            catch (Exception e)
            {
                Logger.WarnException(e, "Unable to Save Image File: {0}", tile);
            }

            return saved;
        }

        public byte[] GetTileImage(Uri uri)
        {
            byte[] content = null;

            try
            {
                if (uri.IsFile)
                {
                    var file = new FileInfo(uri.LocalPath);

                    if (file.Exists && file.Length > 0)
                    {
                        Logger.Debug("Loading Image File... {0}", file.FullName);

                        using (var fs = file.OpenRead())
                        {
                            content = new byte[file.Length];

                            fs.Read(content, 0, content.Length);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.WarnException(e, "Unable to Save Image File: {0}", uri);
            }

            return content;
        }

        public Uri GetStorageUri(TileImageSource source, Tile tile)
        {
            Uri uri = null;

            var file = GetImageFile(source, tile);

            if (file != null && file.Exists)
            {
                uri = new Uri(file.FullName);
            }

            return uri;
        }

        #endregion
    }
}
